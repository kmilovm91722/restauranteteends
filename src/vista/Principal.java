package vista;

import java.util.ArrayList;

import productos.ConstructorBebida;
import productos.ConstructorPlato;
import productos.Director;
import controlingredientes.ControladorCocina;
import controlingredientes.ControladorIngredientes;
import entidades.Bebida;
import entidades.Pedido;
import entidades.Plato;
import entidades.Producto;

public class Principal {
	public static void main(String[] args) {
		// Llega el mesero y dice que pidieron un plato y una bebida
		Director director = new Director();

		ConstructorBebida constructorBebida = new ConstructorBebida();
		ConstructorPlato constructorPlato = new ConstructorPlato();

		director.setConstructorProducto(constructorBebida);

		Bebida bebida = (Bebida) director.getProducto();

		director.setConstructorProducto(constructorPlato);
		Plato plato = (Plato) director.getProducto();

		ArrayList<Producto> productosDelPedido = new ArrayList<Producto>();
		productosDelPedido.add(bebida);
		productosDelPedido.add(plato);

		Pedido pedido = new Pedido(productosDelPedido);

		ControladorCocina controladorCocina = ControladorCocina.getInstance();

		controladorCocina.agregarPedido(pedido);
		// Cocinaron y ha pasado un tiempo
		controladorCocina.despacharPedido();

	}
}
