package entidades;

import java.util.ArrayList;

public class Pedido {
	
	private ArrayList<Producto> productos;
	
	public ArrayList<Producto> getProductos() {
		return productos;
	}

	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;
	}

	public Pedido(ArrayList<Producto> productos){
		this.productos = productos;
	}
	
}
