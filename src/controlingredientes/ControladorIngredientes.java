package controlingredientes;

import java.util.ArrayList;

import entidades.Ingrediente;
import entidades.Pedido;

public class ControladorIngredientes {

	private static ControladorIngredientes instance = null;
	private ArrayList<Ingrediente> ingredientes;

	public synchronized static ControladorIngredientes getInstance() {
		return instance == null ? new ControladorIngredientes() : instance;
	}

	private ControladorIngredientes() {
		cargarIngredientes();
	}

	public ArrayList<Ingrediente> getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(ArrayList<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}

	public void reducirCantidadIngredientes(Pedido pedido) {
		System.out
				.println("Reduciendo la cantidad de ingredientes que consumio el pedido");
	}

	private void cargarIngredientes() {
		System.out
				.println("Carga los ingredientes de la base de datos en el arraylist de ingredientes");
	}

	public void actualizarCantidadIngredientes() {
		System.out
				.println("Guarda en la base de datos el arraylist de ingredientes");
	}

	public void realizarPedidoProveedor(ArrayList<Ingrediente> ingredientes) {
		System.out
				.println("Envia el pedido al proveedor correcto segun cada ingrediente en el ArrayList");
	}
}
