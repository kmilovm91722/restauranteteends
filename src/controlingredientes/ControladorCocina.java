package controlingredientes;

import java.util.LinkedList;
import java.util.Queue;
import entidades.Pedido;

public class ControladorCocina {

	private Queue<Pedido> pedidos;

	private static ControladorCocina instance = null;

	public synchronized static ControladorCocina getInstance() {
		return instance == null ? instance = new ControladorCocina() : instance;
	}

	private ControladorCocina() {
		this.pedidos = new LinkedList<Pedido>();
	}

	public void agregarPedido(Pedido pedido) {
		pedidos.add(pedido);
	}

	public void despacharPedido() {
		Pedido pedido = pedidos.poll();
		ControladorIngredientes.getInstance().reducirCantidadIngredientes(
				pedido);
	}
}
