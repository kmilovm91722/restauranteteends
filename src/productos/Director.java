package productos;

import java.util.ArrayList;

import entidades.Ingrediente;
import entidades.Producto;

public class Director {

	private ConstructorProducto constructorProducto;

	public void setConstructorProducto(ConstructorProducto constructorProducto) {
		this.constructorProducto = constructorProducto;
	}

	public Producto getProducto() {
		return this.constructorProducto.getProducto();
	}

	public void construirProducto(ArrayList<Ingrediente> ingredientes, String nombre) {
		this.constructorProducto.crearNuevoProducto();
		this.constructorProducto.agregarIngredientes(ingredientes);
		this.constructorProducto.colocarNombre(nombre);
	}

}
