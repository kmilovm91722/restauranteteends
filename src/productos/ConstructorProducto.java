package productos;

import java.util.ArrayList;

import entidades.Ingrediente;
import entidades.Producto;

public abstract class ConstructorProducto {

	private Producto producto;
	protected String nombre;

	public Producto getProducto() {
		return producto;
	}

	public abstract void colocarNombre(String nombre);

	public void crearNuevoProducto() {
		this.producto = new Producto();
	}

	public abstract void agregarIngredientes(ArrayList<Ingrediente> ingredientes);

}
