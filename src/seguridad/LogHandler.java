package seguridad;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LogHandler {
	private final static Logger logger = Logger.getLogger("logger.loghandles");
	private final static LogHandler logHandler = new LogHandler();

	private LogHandler() {
		String pathLog = "Log_" + LogHandler.getStrActualDate() + ".log";
		try {
			FileHandler hand = new FileHandler(pathLog);
			logger.addHandler(hand);
			// Disable anothers outputs like console
			logger.setUseParentHandlers(false);
		} catch (IOException e) {
			System.err.println("Error I/O log file: " + pathLog);
			System.exit(1);
		}
	}

	public static LogHandler getLogHandler() {
		return logHandler;
	}

	// get a string with the current time
	private static String getStrActualDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss.SS");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}

	public void sendReport(String report) {
		// Wrapper sendReport, level info by deafult
		sendReport(Level.INFO, report);
	}

	public synchronized void sendReport(Level level, String report) {
		logger.log(level, report);
	}

	public static void main(String argv[]) throws InterruptedException {
		// Ejemplo de como se usa el logger
		LogHandler log = LogHandler.getLogHandler();
		// Llamada al wrapper con level info por defecto
		log.sendReport("Esto es informacion      .... 1");
		// Ejemplo de llamadas con otros niveles
		log.sendReport(Level.WARNING, "Esto es una advertencia       .... 2");
		log.sendReport(Level.OFF, "Esto es una cosa       .... 3");
	}
}